#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<time.h>

#define MAXLEN (200000)
#define OUT_NEU_NUM (26)
#define HID_NEU_NUM (80)
#define INP_NEU_NUM (203)
#define DEBUG (0)
struct neural {
    double sum ;
    double gradient ;
    double s_out ;
};

struct weight {
    double weight ;
    double delta_weight ;
};

struct data {
    char vol[MAXLEN]     ;
    char phoneme[MAXLEN] ;
    char stress[MAXLEN]  ;
};

typedef struct neural Neural ;
typedef struct weight Weight ;
typedef struct data   Data   ;

Neural n_cor[OUT_NEU_NUM]  ;
Neural n_out[OUT_NEU_NUM]  ;
Neural n_hid[HID_NEU_NUM + 1]  ; // add one bias 
Neural n_inp[INP_NEU_NUM + 1]  ; // add one bias
Weight w_i_h[INP_NEU_NUM + 1][HID_NEU_NUM]  ;
Weight w_h_o[HID_NEU_NUM + 1][OUT_NEU_NUM]  ;

Data trainData = {{0}} ;
Data testData  = {{0}} ;

double fRand(double fMin, double fMax){
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}
double sigmod(double input){
    return 1. / (1. + exp(-input)) ;
}
double dsigmod(double input){
    return exp(- input) / ((exp(- input) + 1.) * (exp(-input) + 1.))  ;
}
int fcn_compare(const void * a ,const void * b){
    if ( (( int *)a)[1] < (( int *)b)[1] ){
        return -1 ;
    }else{
        return  1 ;
    }
    
}

void update(){
    int node,p_node ;
    double sum = 0.0 ;
    // layer input -> layer hidden
    for ( node = 0 ; node < HID_NEU_NUM ; node++ ){
        sum = 0.0 ;
        for ( p_node = 0 ; p_node < (INP_NEU_NUM + 1) ; p_node++ ){
            sum += n_inp[p_node].s_out * w_i_h[p_node][node].weight ;
        }
        n_hid[node].sum = sum ;
        n_hid[node].s_out = sigmod(sum) ; 
    }
    // layer hidden -> layer output
    for ( node = 0 ; node < OUT_NEU_NUM ; node++ ){
        sum = 0.0 ;
        for ( p_node = 0 ; p_node < ( HID_NEU_NUM + 1 ) ; p_node++ ){
            sum += n_hid[p_node].s_out * w_h_o[p_node][node].weight ;
        }
        n_out[node].sum = sum ;
        n_out[node].s_out = sigmod(sum) ;
    }
    return ;
}
void gradient(){
    int node , p_node , i ;
    double temp;
    for (node = 0 ; node < OUT_NEU_NUM ; node++ ){
        n_out[node].gradient = ( n_cor[node].s_out - n_out[node].s_out ) * dsigmod( n_out[node].sum ) ; 
    }
    for (p_node = 0 ; p_node < ( HID_NEU_NUM + 1 ) ; p_node++ ){
        temp = 0.0 ;
        for ( node = 0 ; node < OUT_NEU_NUM ; node++ ){
            temp += n_out[node].gradient * w_h_o[p_node][node].weight * dsigmod( n_hid[p_node].sum ) ;
        }
        n_hid[p_node].gradient = temp ;
    }
    for (p_node = 0 ; p_node < ( INP_NEU_NUM + 1 ) ; p_node++ ){
        temp = 0.0 ;
        for ( node = 0 ; node < HID_NEU_NUM ; node++ ){
            temp += n_hid[node].gradient * w_i_h[p_node][node].weight * dsigmod( n_inp[p_node].sum ) ;
        }
        n_inp[p_node].gradient = temp ;
    }    
    
    if (DEBUG == 1){
        printf( "COR : " );
        for ( i=0 ; i< 26 ; i++){
            printf( "%3.1f ", n_cor[i].s_out ) ;
        }    
        printf( "\n" );
        printf( "OUT : " );
        for ( i=0 ; i< 26 ; i++){
            printf( "%3.1f ", n_out[i].s_out ) ;
        }    
        printf( "\n" );    
    }
}
void delta_w(){
    double alpha = 0.95   ;
    int    node , p_node ; 
      
    for ( p_node = 0 ; p_node < ( HID_NEU_NUM + 1 ) ; p_node++ ){
        for ( node = 0 ; node < OUT_NEU_NUM ; node++ ){
            w_h_o[p_node][node].delta_weight = 
                    alpha           *  w_h_o[p_node][node].delta_weight
                +  ( 1.0 - alpha )  *  n_out[node].gradient * n_hid[p_node].s_out ; 
        }
    }
    for ( p_node = 0 ; p_node < ( INP_NEU_NUM + 1 ) ; p_node++ ){
        for ( node = 0 ; node < HID_NEU_NUM ; node++ ){
            w_i_h[p_node][node].delta_weight =
                    alpha           *  w_i_h[p_node][node].delta_weight
                +  ( 1.0 - alpha )  *  n_hid[node].gradient * n_inp[p_node].s_out ;             
        }
    }
    
}
void update_w(){
    double epsilon = 1.0 ;  
    int node,p_node ;
  
    for ( p_node = 0 ; p_node < ( HID_NEU_NUM + 1 ) ; p_node++ ){
        for ( node = 0 ; node < OUT_NEU_NUM ; node++ ){
            w_h_o[p_node][node].weight += epsilon * w_h_o[p_node][node].delta_weight ;
        }
    }
    for ( p_node = 0 ; p_node < ( INP_NEU_NUM + 1 ) ; p_node++ ){
        for ( node = 0 ; node < HID_NEU_NUM ; node++ ){
            w_i_h[p_node][node].weight += epsilon * w_i_h[p_node][node].delta_weight ;
        }
    }
}
void mapword(int groupnum,char word){
    int i ;
    for ( i=0 ; i< 29 ; i++ ){
        n_inp[groupnum*29 + i].s_out =  0.0 ;
        n_inp[groupnum*29 + i].sum   = -5.0 ; 
    }
    if ( word != '_' ){
        n_inp[groupnum*29 + word - 97].s_out = 1.0 ;
        n_inp[groupnum*29 + word - 97].sum = 5.0 ;
    }else{
        n_inp[groupnum*29 + 26].s_out = 1.0 ;
        n_inp[groupnum*29 + 26].sum   = 5.0 ;       
    }
    return ;
}    
void get_teacher(int current_end){
    int i = 0 ;
    char ans_phoneme_char = trainData.phoneme[current_end-3] ;
    char ans_stress_char  = trainData.stress[current_end-3] ;
    int cal_phoneme       = (int)ans_phoneme_char ;
    int cal_stress        = (int)ans_stress_char ;
    
    for ( i=0 ; i< OUT_NEU_NUM ; i++){
        n_cor[i].s_out = 0.0 ;
    }
    for ( i=0 ; i<8 ; i++){
        n_cor[i].s_out   = (cal_phoneme % 2 == 0) ?  0.0 : 1.0 ;
        n_cor[i].sum     = (cal_phoneme % 2 == 0) ? -5.0 : 5.0 ;
        cal_phoneme = cal_phoneme >> 1 ;
    }
    for ( i=0 ; i<8 ; i++ ){
        n_cor[i+8].s_out = (cal_stress  % 2 == 0) ?  0.0 : 1.0 ;
        n_cor[i+8].sum   = (cal_stress  % 2 == 0) ? -5.0 : 5.0 ;
        cal_stress = cal_stress >> 1 ;
    }
    
    
    for ( i=0 ; i< 26 ; i++){
        //printf( "%3.1f ", n_cor[i].s_out ) ;
    }    
    //printf( "\n" );
    
    return ;
}
void init(){
    int node , p_node ;
    
    for ( node = 0 ; node < OUT_NEU_NUM ; node++){
        n_cor[node].sum      = 0.0 ;
        n_cor[node].gradient = 0.0 ;
        n_cor[node].s_out    = 0.0 ;
        n_out[node].sum      = 0.0 ;
        n_out[node].gradient = 0.0 ;
        n_out[node].s_out    = 0.0 ;
        for ( p_node = 0 ; p_node < (HID_NEU_NUM + 1) ; p_node++){
            w_h_o[p_node][node].weight       = fRand(-0.3,0.3) ;
            w_h_o[p_node][node].delta_weight = 0.0 ;
        }
    }    
    for ( node = 0 ; node < HID_NEU_NUM ; node++){
        n_hid[node].sum      = 0.0 ;
        n_hid[node].gradient = 0.0 ;
        n_hid[node].s_out    = 0.0 ;
        for ( p_node = 0 ; p_node < (INP_NEU_NUM + 1) ; p_node++){
            w_i_h[p_node][node].weight        = fRand(-0.3,0.3) ;
            w_i_h[p_node][node].delta_weight  = 0.0 ;
        }
    }    
    for ( node = 0 ; node < INP_NEU_NUM ; node++){
        n_inp[node].sum      = 0.0 ;
        n_inp[node].gradient = 0.0 ;
        n_inp[node].s_out    = 0.0 ;
    }
    
    // Handle the bias neural
     n_hid[HID_NEU_NUM].sum      = -5.0  ;
     n_hid[HID_NEU_NUM].gradient =  0.0  ;
     n_hid[HID_NEU_NUM].s_out    = -1.0  ;
     n_inp[INP_NEU_NUM].sum      = -5.0  ;
     n_inp[INP_NEU_NUM].gradient =  0.0  ;
     n_inp[INP_NEU_NUM].s_out    = -1.0  ;
     
}
void read_ans(){
    int  temp , i ;
    char vol[30]     = {0} ;
    char phoneme[30] = {0} ;
    char stress[30]  = {0} ;
    
    char vol_temp[50000][40]     = {{0}} ;
    char phoneme_temp[50000][40] = {{0}} ;
    char stress_temp[50000][40]  = {{0}} ;
    int  shuffle_arr[50000][2]   = {{0}} ;
    int  cur_idx ; 
    
    FILE * fp = NULL ; 
    
    //printf("reading Training\n");
    fp = fopen("oridata","r") ;
    fscanf(fp,"%s%s%s%d", vol , phoneme , stress , &temp);
    cur_idx = 0 ;
    while( !feof(fp) ){
        strcpy( vol_temp[cur_idx]     , vol     ) ;
        strcpy( phoneme_temp[cur_idx] , phoneme ) ;
        strcpy( stress_temp[cur_idx]  , stress  ) ;
        shuffle_arr[cur_idx][0] = cur_idx ;
        shuffle_arr[cur_idx][1] = rand() ;
        cur_idx ++ ;
        fscanf(fp,"%s%s%s%d", vol , phoneme , stress , &temp);
    }
    
    qsort( shuffle_arr , cur_idx , 2 * sizeof(int) , fcn_compare ) ;
    for ( i = 0 ; i < 100 ; i++ ) {
        strcpy( vol     , vol_temp[ shuffle_arr[i][0] ]     ) ;
        strcpy( phoneme , phoneme_temp[ shuffle_arr[i][0] ] ) ;
        strcpy( stress  , stress_temp[ shuffle_arr[i][0] ]  ) ;
        strcat(trainData.vol,"_");
        strcat(trainData.vol,vol);
        strcat(trainData.phoneme,"_");
        strcat(trainData.phoneme,phoneme);
        strcat(trainData.stress,"_");
        strcat(trainData.stress,stress);
    }
    fclose(fp);
    
    //printf("reading Testing\n");
    fp = fopen("list","r") ;
    fscanf(fp,"%s%s%s%d", vol , phoneme , stress , &temp);
    while ( !feof(fp) ){
        //printf("reading words :%s\n" ,stress);
        strcat(testData.vol,"_");
        strcat(testData.vol,vol);
        strcat(testData.phoneme,"_");
        strcat(testData.phoneme,phoneme);
        strcat(testData.stress,"_");
        strcat(testData.stress,stress);
        fscanf(fp,"%s%s%s%d", vol , phoneme , stress , &temp);
    }
    fclose(fp);    
    return ;
}
void load_word(int current_end,int mode){
    int i=0 , groupnum=0 ;
    for ( i=current_end - 6 ; i<=current_end ; i++){
        if (mode == 0){
            mapword(groupnum,trainData.vol[i]);
        }else{
            mapword(groupnum,testData.vol[i]);
        }
        groupnum++ ;
    }
    return ;
}
void check_answer(){
    char phoneme_sheet[1000] = {0} ;
    char stress_sheet[1000]  = {0} ;
    strcpy(phoneme_sheet,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#*^+-_");
    strcpy(stress_sheet ,"><021_");
    int len_phoneme  = strlen(phoneme_sheet) ;
    int len_stress   = strlen(stress_sheet)  ;
    int len_testData = strlen(testData.vol)  ;
    int test_end = 6 ;
    
    double eu_dist_phoneme ;
    double eu_dist_stress  ;
    double temp_dist = 0.0 ;

    int cal_phoneme ;
    int cal_stress  ;
    
    char phoneme_guess ;
    char stress_guess  ;
    
    int i,node ;
    
    int bit_value ; 
    
    int correct   = 0 ;
    int correct_p = 0 ;
    int correct_s = 0 ;
    int compare   = 0 ;
    while ( test_end < len_testData ){
        load_word(test_end,1) ;
        update()   ;
        // Calculate phoneme guess
        eu_dist_phoneme = 10000000.0 ;
        for ( i = 0 ; i < len_phoneme ; i++ ){
            cal_phoneme = (int)phoneme_sheet[i] ;
            temp_dist = 0.0 ;
            for ( node = 0 ; node < 8 ; node ++ ){
                bit_value  = (cal_phoneme % 2 == 0) ? 0.0 : 1.0 ;
                temp_dist += ( bit_value - n_out[node].s_out ) * ( bit_value - n_out[node].s_out ) ;
                cal_phoneme = cal_phoneme >> 1 ; 
            }
            if ( temp_dist <= eu_dist_phoneme ){
                eu_dist_phoneme = temp_dist ;
                phoneme_guess = phoneme_sheet[i] ;
            }
        }
        
        // Calculate stress guess
        eu_dist_stress  = 10000000.0 ;       
        for ( i = 0 ; i < len_stress ; i++ ){
            cal_stress = (int)stress_sheet[i]  ;
            temp_dist = 0.0 ;
            for ( node = 8 ; node < 16 ; node ++ ){
                bit_value  = (cal_stress % 2 == 0) ? 0.0 : 1.0 ;
                temp_dist += ( bit_value - n_out[node].s_out ) * ( bit_value - n_out[node].s_out ) ;
                cal_stress = cal_stress >> 1 ; 
            }
            if ( temp_dist <= eu_dist_stress ){
                eu_dist_stress = temp_dist ;
                stress_guess = stress_sheet[i] ;
            }
        }
        
        
        if ( phoneme_guess == testData.phoneme[test_end-3]){
            correct_p ++ ;
            if (DEBUG == 2){
                if ( phoneme_guess != '_' )
                    printf("phoneme : %c == %c\n",phoneme_guess,testData.phoneme[test_end-3]);
            }
        }
        
        if ( stress_guess  == testData.stress[test_end-3] ){
            correct_s ++ ;
            if (DEBUG == 2){
                if ( stress_guess != '_' )
                    printf("stress  : %c == %c \n",stress_guess,testData.stress[test_end-3]);
            }
        }
        
        if ( phoneme_guess == testData.phoneme[test_end-3] && stress_guess  == testData.stress[test_end-3] ){
            correct ++ ;
        }
        test_end ++ ;
        compare  ++ ;
    }

    //printf("compare = %d , correct = %d , rate = %f , rate_p = %f , rate_s = %f \n" ,
    printf(" %f %f %f \n" ,
        (double)correct   / (double)compare * 100. ,
        (double)correct_p / (double)compare * 100. ,
        (double)correct_s / (double)compare * 100. 
    ) ;
    fflush(stdout) ; 
    return ;    
}

int main(){
    int pass ;
    int current_end = 6 , test_count = 0   ; //index of list node , start at 6
    int length_word ;
    
    srand(1);  //(unsigned int) time(NULL) );
    
    read_ans() ;
    init();
    length_word = strlen(trainData.vol) ;
    current_end = 6 ;
    for ( pass = 0 ; pass < 100 ; ++pass ){
        if ( current_end >= length_word ){
            current_end = 6 ; 
        }
        
        //printf("pass = %d \n" ,pass);
        
        test_count = 0 ;
        while ( current_end < length_word && test_count < 100000 ){
            if ( current_end % 1000 == 0 ){
                //printf("current_end = %d \n" ,current_end);
            }
            load_word(current_end,0) ;
            get_teacher(current_end) ;
            update()   ;       
            gradient() ;
            delta_w()  ;
            update_w() ;
            current_end ++  ;
            test_count ++ ;
        }
        check_answer() ;
    }
    
    
    return 0;
}
