
CC=gcc
LD=-lm
OUT=main.exe

all:main.c
	$(CC) main.c -o $(OUT) $(LD)

clean:
	rm -rf *.exe *~
